import axios from 'axios';
import { decideENV } from './decideENV';

//  192.168.100.11:7222 => local
export const ServiceInstance = axios.create({
    baseURL:
        decideENV() === 'DEV'
            ? process.env.REACT_APP_BaseURL_DEV
            : process.env.REACT_APP_BaseURL_PROD,
    headers: {
        'eO2-Secret-Code': process.env.REACT_APP_EO2_SECRET_CODE,
        'Content-Type': 'multipart/form-data',
    },
});

export const SubmitServiceInstance = axios.create({
    baseURL:
        decideENV() === 'DEV'
            ? process.env.REACT_APP_BaseURL_DEV
            : process.env.REACT_APP_BaseURL_PROD,
    headers: {
        'eO2-Secret-Code': process.env.REACT_APP_EO2_SECRET_CODE,
        'Content-Type': 'application/json',
    },
});
