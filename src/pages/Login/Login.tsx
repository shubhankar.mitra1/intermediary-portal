/* eslint-disable no-nested-ternary */
import { useState, useRef } from 'react';
import { Button, Card, Form, InputGroup } from 'react-bootstrap';
import { EoxegenLogoColour, LoginBackground } from '@assets/index';
import { useAuth } from '@hooks/useAuth';
import './styles.css';
import { LoginAPI } from '@api/login.api';
import { Navigate } from 'react-router-dom';
import Spinner from '@components/Spinner/Spinner';

type LoginFunc = {
    login?: (args: object) => void;
    user?: object | null;
};

const Login = () => {
    const [openLicense, setOpenLicense] = useState(false);
    const [isLoggedIn, setIsLoggedIn] = useState<boolean | undefined>(undefined);
    const emailRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);
    const { login, user }: LoginFunc = useAuth();

    if (user) {
        return <Navigate to="/home" />;
    }

    const handleSubmit = async () => {
        const details = { email: emailRef.current?.value, password: passwordRef.current?.value };
        // login api to be called and the result should be saved in login context
        setIsLoggedIn(false);
        if (!details.email || !details.password) {
            console.log('Fill all the fields');
            setIsLoggedIn(undefined);

            return;
        }

        const timeout = setTimeout(async () => {
            const LoginDetails = await LoginAPI(details);
            if (LoginDetails.loggedIn) {
                if (login) {
                    login(LoginDetails);
                    setIsLoggedIn(true);
                }
            } else {
                console.log(LoginDetails.error);
                setIsLoggedIn(undefined);
            }

            clearTimeout(timeout);
        }, 0);
    };

    return (
        <main className="login--main">
            <section className="first--half">
                {/* <EoxegenLogo /> */}
                {/* <span className="text-white font-25 ps-3">eOxegen</span> */}
                <LoginBackground />
            </section>
            <section className="login--card">
                <Card className="login_details_card border-0 rounded rounded-lg" style={{ width: '35rem' }}>
                    <Card.Title className="text-center">
                        <strong className="sign-in-header">
                            Welcome to <EoxegenLogoColour />
                        </strong>
                    </Card.Title>
                    <Card.Body>
                        <Form className="mt-4">
                            <Form.Label>USERID / MOBILE / EMAIL</Form.Label>
                            <Form.Control
                                ref={emailRef}
                                type="text"
                                className="rounded rounded-lg"
                                onKeyDown={(e) => {
                                    if (e.key === 'Enter') {
                                        handleSubmit();
                                    }
                                }}
                                required
                            />
                            <Form.Label className="mt-4">PASSWORD</Form.Label>
                            <Form.Control
                                ref={passwordRef}
                                type="password"
                                className="rounded rounded-lg"
                                onKeyDown={(e) => {
                                    if (e.key === 'Enter') {
                                        handleSubmit();
                                    }
                                }}
                                required
                            />
                            <section className="options">
                                <Form.Label className="mt-4">
                                    <InputGroup>
                                        <div className="rememberBox">
                                            <div>
                                                <InputGroup.Checkbox />
                                            </div>
                                            <div>Remember Me</div>
                                        </div>
                                    </InputGroup>
                                </Form.Label>
                                <a href="/" onClick={(e) => e.preventDefault()}>
                                    Forgot Password
                                </a>
                            </section>
                            <Button disabled={isLoggedIn === false} className="mt-2 rounded rounded-lg w-100 btn-bg" onClick={handleSubmit}>
                                {isLoggedIn === undefined ? 'Sign In' : isLoggedIn === false ? <Spinner suspense={false} /> : null}
                            </Button>

                            <hr />
                        </Form>
                    </Card.Body>
                </Card>
            </section>
        </main>
    );
};

export default Login;
