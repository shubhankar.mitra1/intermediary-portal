import { CustomisedTable } from '@components/CustomisedTable/CustomisedTable';
import { Card, Row, Col, Container, Form } from 'react-bootstrap';

const Claims = () => {
    return (
        <Container fluid>
            <p className="font-16 section--name">CLAIMS</p>
            <Row>
                <Card className="border-0 p-3">
                    <Card.Body style={{ padding: '0' }}>
                        <p className="font-16 section--name">Check Claim Status</p>
                        <Form.Control type="text" placeholder="Enter Claim Number" />
                    </Card.Body>
                </Card>
            </Row>
            <Row className="mt-4">
                <Card className="border-0 p-3">
                    <p className="font-16 section--name">List of claims made</p>
                    <Card className="border-0">
                        <p className="font-16 section--name">Search Filter</p>
                        <Card.Body style={{ padding: '0 0 1rem 0' }}>
                            <Row>
                                <Col md={4}>
                                    <Form.Label className="font-14 fw-bold text-muted">Customer Name</Form.Label>

                                    <Form.Control type="text" placeholder="Choose..." />
                                </Col>
                                <Col md={4}>
                                    <Form.Label className="font-14 fw-bold text-muted">Customer Name</Form.Label>

                                    <Form.Control type="text" placeholder="Choose..." />
                                </Col>
                                <Col md={4}>
                                    <Form.Label className="font-14 fw-bold text-muted">Customer Name</Form.Label>

                                    <Form.Select>
                                        <option>Open this select menu</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </Form.Select>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                    <CustomisedTable count={1} />
                </Card>
            </Row>
        </Container>
    );
};

export default Claims;
