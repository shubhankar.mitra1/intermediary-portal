import Chart from 'react-apexcharts';
import { BarChartType } from 'Types/types';
import { ApexOptions } from 'apexcharts';

export const Bar = ({ options, series, width, height }: BarChartType) => {
    return <Chart options={options} series={series} type="bar" width={width} height={height} />;
};

export const BarGeneralized = (props: ApexOptions) => {
    return <Chart {...props} />;
};

export const Pie = (props: ApexOptions) => {
    return <Chart {...props} />;
};

export const Area = (props: ApexOptions) => {
    return <Chart {...props} />;
};
