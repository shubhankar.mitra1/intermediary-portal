import './styles.css';

type PropType = {
    suspense: boolean;
};

const Spinner = ({ suspense = false }: PropType) => {
    return (
        <div className="loader" style={{ visibility: `${suspense ? 'hidden' : 'visible'}` }}>
            {' '}
        </div>
    );
};

export default Spinner;
