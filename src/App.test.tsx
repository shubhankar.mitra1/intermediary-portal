import { render, screen } from '@testing-library/react';
import { describe, it } from 'vitest';
import App from './App';

describe('App', () => {
    it('Renders hello World', () => {
        // Arrange
        render(<App />);
        // ACT
        // Expect
        expect(
            screen.getByRole('heading', {
                level: 1,
            })
        ).toHaveTextContent('Hello world!');
    });
});
