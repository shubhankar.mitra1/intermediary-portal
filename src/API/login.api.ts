// import { ServiceInstance } from '../../axiosConfig';

// export const Login = async (payLoad: object) => {
//     let result;
//     try {
//         result = await ServiceInstance.post(
//             '/validateproviderlogin',
//             { requiredOTP: 'Y', ...payLoad } /* Object.assign({ requiredOTP: 'Y' }, payLoad) */
//         );
//         return { ok: true, data: result, error: null };
//     } catch (error) {
//         return { ok: false, data: null, error };
//     }
// };

type PayLoadType = {
    email?: string;
    password?: string;
};

export const LoginAPI = async (payLoad: PayLoadType) => {
    if (payLoad.email === 'admin@admin.com' && payLoad.password === 'admin') {
        return { ...payLoad, loggedIn: true, error: null };
    }
    return { ...payLoad, loggedIn: false, error: 'Wrong username/password combination' };
};
