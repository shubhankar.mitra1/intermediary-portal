type TreeProp = {
    children: JSX.Element | string;
};

const SubLayout = ({ children }: TreeProp) => {
    return <div style={{ padding: '1.5rem' }}>{children}</div>;
};

export default SubLayout;
