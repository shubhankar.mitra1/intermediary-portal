import Header from './Header/Header';
import SubLayout from './SubLayout';
import './styles.css';
import Sidebar from './Sidebar/Sidebar';

type TreeProp = {
    children: JSX.Element;
};

const Layout = ({ children }: TreeProp) => {
    return (
        <div className="background">
            {/* <Header /> */}
            <Sidebar />
            <section className="second--section">
                <Header />
                <SubLayout>{children}</SubLayout>
            </section>
        </div>
    );
};

export default Layout;
