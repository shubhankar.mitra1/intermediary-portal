import './App.css';
import { lazy, Suspense } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Spinner from '@components/Spinner/Spinner';
import { ProtectedRoute } from '@components/ProtectedRoute/ProtectedRoute';
import { AuthProvider } from '@hooks/useAuth';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const Layout = lazy(() => import('@layout/Layout'));
const Login = lazy(() => import('@pages/Login/Login'));
const Home = lazy(() => import('@pages/Home/Home'));
const Customer = lazy(() => import('@pages/Customer/Customer'));
const Claims = lazy(() => import('@pages/Claims/Claims'));
const Policies = lazy(() => import('@pages/Policies/Policies'));
const Endorsement = lazy(() => import('@pages/Endorsement/Endorsement'));
const ComissionStatement = lazy(() => import('@pages/ComissionStatement/ComissionStatement'));

function App() {
    return (
        <main>
            <Routes>
                <Route
                    path="/home"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Home />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />

                <Route
                    path="/customer"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Customer />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/claims"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Claims />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/policies"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Policies />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/endorsement"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <Endorsement />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/comissionstatement"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <ComissionStatement />
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />

                <Route
                    path="/"
                    element={
                        <Suspense fallback={<Spinner suspense />}>
                            <Login />
                        </Suspense>
                    }
                />
                <Route
                    path="/*"
                    element={
                        <ProtectedRoute>
                            <Layout>
                                <Suspense fallback={<Spinner suspense />}>
                                    <h1>Not Found</h1>
                                </Suspense>
                            </Layout>
                        </ProtectedRoute>
                    }
                />
            </Routes>
        </main>
    );
}

const queryClient = new QueryClient();

const WrappedApp = () => {
    return (
        <QueryClientProvider client={queryClient}>
            <BrowserRouter>
                <AuthProvider>
                    <App />
                </AuthProvider>
            </BrowserRouter>
        </QueryClientProvider>
    );
};

export default WrappedApp;
