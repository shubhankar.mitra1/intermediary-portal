export const injectID = (arr: any = []) => {
    return arr.map((item: object) => {
        if (typeof item === 'object' && item instanceof Object) {
            return { data: { ...item }, id: crypto?.randomUUID?.() };
        }
        if (typeof item === 'string' || typeof item === 'number') {
            return { data: item, id: crypto?.randomUUID?.() };
        }
        return [];
    });
};
