import { ApexOptions } from 'apexcharts';

export type UseAuthType = {
    login?: (args: object) => void;
    user?: object | null;
    logout?: () => void;
};

export type ClaimsCardType = {
    name?: string;
    date?: string;
    context?: string;
    status?: string;
    amount?: string;
    displayPic?: any;
};

export type BarChartType = {
    options?: object;
    series?: ApexOptions['series'];
    width?: string;
    height?: string;
};

export type ClaimsSummaryCardType = {
    name?: string;
    location?: string;
    country?: string;
    contact?: string;
    dateofservice?: string;
    relation?: string;
    reason?: string;
    claimtype?: string;
    coveragetype?: string[];
    networktype?: string;
    status?: string;
    hraclaimno?: string;
    amountbilled?: string;
    amountdeducted?: string;
    plainpaid?: string;
    paymentmode?: string;
    paymentdetails?: string;
};

export type PlanBalancesCardType = {
    name?: string;
    figure?: ApexOptions;
    table: object;
};

export type DocumentCardType = {
    name?: string;
    companyname?: string;
    issuedate?: string;
};

export type PolicyCardType = {
    policynumber?: string;
    scheme?: string;
    amount?: string;
    renewdate?: string;
    policystartdate?: string;
    policyterm?: string;
    membercount?: number;
};

export type ProviderCardType = {
    name?: string;
    rating?: number;
    image?: any;
    networkstatus?: string;
    address?: string;
    contact?: string;
    distance?: string;
    lat?: string;
    long?: string;
    url?: string;
};
